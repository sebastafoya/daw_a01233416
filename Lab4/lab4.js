function generateTable(size) {

    const appRoot = document.getElementById("table");

    const table = document.createElement("table");

    for (let i = 1; i <= size; i++) {

        const row = table.insertRow(-1);

        for (let j = 1; j <= 3; j++) {
            const cell = row.insertCell(j-1);
            cell.innerHTML = Math.pow(i, j);
        }
    }

    table.setAttribute("border", "2");

    appRoot.appendChild(table);
}

function askForResult() {

    const num1 = Math.floor(Math.random() * 50);
    const num2 = Math.floor(Math.random() * 50);

    const start = new Date().getTime();

    const answer = prompt("Ejercicio 2: ¿Cuál es la suma de "+num1+" y "+num2+"?");

    const now = new Date().getTime();
    const answerTime = (now - start) / 1000;

    const message = answer == num1+num2 ? "Correcto!" : "Incorrecto!";
    alert(message + "\n" + "Tiempo transcurrido: " + answerTime + " s.");
}

function countNegatives(inputArray) {

    const appRoot = document.getElementById("counter");

    let negatives = 0;
    let zeros = 0;
    let aboveZero = 0;
    for (let i = 0; i < inputArray.length; i++){
        const num = inputArray[i];
        if (num < 0) {
            negatives++;
        } else if (num == 0) {
            zeros++;
        } else {
            aboveZero++;
        }
    }

    const p = document.createElement("p");
    p.innerText = "Negativos: "+negatives+"   Ceros: "+zeros+"   Mayores a cero: "+aboveZero;

    appRoot.appendChild(p);

    return {
        negatives: negatives,
        zeros: zeros,
        aboveZero: aboveZero
    }
}

function matrixAverages(matrix) {

    const appRoot = document.getElementById("promedios");

    let averages = []

    for (let i = 0; i < matrix.length; i++) {
        values = matrix[i];
        let sum = values.reduce((previous, current) => current += previous);
        let avg = sum / values.length;
        averages[i] = avg;
    }

    const p = document.createElement("p");
    p.innerText = averages;

    appRoot.appendChild(p);

    return averages;
    
}

function reverseNumber(n) {
    const appRoot = document.getElementById("reversed");
    n = n + "";

    reversed = n.split("").reverse().join("");

    const p = document.createElement("p");
    p.innerText = reversed;

    appRoot.appendChild(p);

    return reversed;
}

class Triangle {
    constructor(a, b, c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    perimeter() {
        return this.a + this.b + this.c;
    }
};

function renderPerimeter(a, b, c) {

    const appRoot = document.getElementById("triangle");

    const triangle = new Triangle(2, 3, 5);
    
    const perimeter = triangle.perimeter();

    const p = document.createElement("p");
    p.innerText = "Perímetro: " + perimeter;

    appRoot.appendChild(p);
}

const size = prompt("Ingresa un número:");
generateTable(size);

askForResult();

countNegatives([-1, 0, 5, 6, -8]);
countNegatives([50, -2, -5, 0, 0]);

matrixAverages([[0, 1, 5], [2, 8, 0]]);
matrixAverages([[100, 100, 400], [22, 83, 10]]);

reverseNumber(8201);
reverseNumber(919193);

const triangle = new Triangle(2, 3, 5);
renderPerimeter(triangle);