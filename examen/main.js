function showNewZombie() {
    $("#addZombie").show();
}

function fetchZombies() {
    $.get("model.php?command=getAllZombies", function(data) {
        $("#zombiesTable").html(data)
    })
}

function addZombie() {
    $.post("model.php?command=addZombie", function(data) {
        console.log(data)
    });
    $("#addZombie").hide();
}