CREATE TABLE zombies (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name varchar(100)
)

CREATE TABLE estados (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    estado varchar(100)
)

CREATE TABLE zombies_estados (
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_zombie int,
    id_estado int,
    FOREIGN KEY (id_zombie) REFERENCES zombies(id),
    FOREIGN KEY (id_estado) REFERENCES estados(id)
)