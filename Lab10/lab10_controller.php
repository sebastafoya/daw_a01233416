<?php

function _e($string)
    {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    }

$users = array(
    "sebastafoya@gmail.com" => "itesmqro$65",
    "diegogonzalez@gmail.com" => "itesmqro$66",
);

$message="message";

if (isSet($_POST['account']) && isSet($_POST['psw'])) {
    $account = _e($_POST['account']);
    $password = _e($_POST['psw']);

    $auth = false;
    try {
        $auth = $users[$account] == $password;
    } catch (Exception $e) {
        $error = $e->getMessage();
    }

    if ($auth == true) {
        $message = "Hi ".$account.", you are successfully authenticated!";
    } else {
        $message = "Invalid Username or Password!";
    }

    include("response_view.php");
}

?>