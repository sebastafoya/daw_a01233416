-- Stored procedure en PostgreSQL para obtener una lista de asistencias en base a un rango de fechas y horarios.

REATE OR REPLACE FUNCTION get_attendance_history(lower_date DATE, upper_date DATE, lower_time TIME, upper_time TIME)
RETURNS TABLE(
	fecha DATE,
	tiempo TIME,
	nombre VARCHAR(255),
	apellido VARCHAR(255),
	estado int
)
AS $$
BEGIN
	RETURN QUERY
	SELECT asistencia.fecha, asistencia.tiempo, usuario.nombre, usuario.apellido, asistencia.estado 
    FROM asistencia, usuario
    WHERE 
    asistencia.cliente = usuario.ID AND 
    asistencia.fecha >= lower_date AND asistencia.fecha <= upper_date AND
    asistencia.tiempo >= lower_time and asistencia.tiempo <= upper_time
    ORDER BY asistencia.fecha DESC;
END;
$$ LANGUAGE 'plpgsql';