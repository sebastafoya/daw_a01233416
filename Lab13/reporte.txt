Agrega un nuevo registro a la tabla de materiales:
  INSERT INTO Materiales values(1000, 'xxx', 1000)

Revisa el contenido de la tabla materiales y determina si existe alguna inconsistencia en el contenido de la tabla. �Cu�l es? �A qu� se debe? 
Ocurri� una violaci�n debido a que la clave ya existe en la tabla, porque ya hab�a marcado la clave como llave primaria al crear la tabla.

Agrega nuevamente el registro a la tabla de materiales:
  INSERT INTO Materiales values(1000, 'xxx', 1000)

�Qu� ocurri�?
Lo mismo, una violaci�n.

Verifica si la tabla de materiales tiene definido alg�n constraint (restricci�n):
  sp_helpconstraint materiales

�Qu� informaci�n muestra esta consulta?
Los contraints asignados a la tabla. En este caso, un contraint PRIMARY KEY.

Aseg�rate de crear constraints an�logos para definir las llaves primarias de las tablas proveedores, y proyectos.

�Qu� sentencias utilizaste para definir las llaves primarias?
RFC char(13) PRIMARY KEY NOT NULL
Numero numeric(5) PRIMARY KEY NOT NULL

Define el constraint correspondiente a la llave primaria para la tabla entregan, considerando que en este caso, la llave primaria est� integrada por varias columnas.

�Qu� sentencias utilizaste para definir este constrait?
PRIMARY KEY CLUSTERED (Clave, RFC, Numero, Fecha)

Intenta insertar en la tabla entregan el siguiente registro:
  INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0) ;

�Qu� particularidad observas en los valores para clave, rfc y numero? �C�mo responde el sistema a la inserci�n de este registro?
Debida a que ya hab�a agregado los contraints FOREIGN KEY a mis tabla, ocurri� una violaci�n en la clave, debido a que no existen las llaves en las tablas a las
cuales se hace referencia.

Ahora agreguemos el siguiente constraint:
  ALTER TABLE entregan add constraint cfentreganclave
  foreign key (clave) references materiales(clave);

Intenta nuevamente la inserci�n del registro inconsistente.
�Qu� significa el mensaje que emite el sistema? �Qu� significado tiene la sentencia anterior? 
Que ocurri� un conflicto debido a que no es posible usar una referencia con la llave for�nea introducida, porque no existe en la otra tabla.

Revisa los constraints de cada tabla. Para visualizar los constraints que hemos creado, se utiliza la siguiente sentencia:
  sp_helpconstraint tableName
�Qu� significan las columnas de esas consultas? 
	contraint_type - El tipo de constraint
	contraint_name - El nombre asignado al contraint
	delete_action - Qu� hacer en caso de eliminar un registro en la tabla
	update_action - Qu� hacer en caso de modificar un registro en la tabla
	status_enabled - Estatus del contraint
	status_for_replication - Si el contraint est� listo para ser replicado
	contraint_keys - Las llaves que forman parte del contraint

Efect�a la siguiente sentencia INSERT:
  INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0);
Lista el registro que acabas de crear.
�Qu� uso se le est� dando a GETDATE()? �Tiene sentido el valor del campo de cantidad?
GETDATE() se utiliza para introducir la fecha de hoy. No, la cantidad no tiene sentido porque no puede existir una
entrega con cero materiales.

Intenta insertar un registro con cantidad igual o menor que 0.
�C�mo responde el sistema? �Qu� significa el mensaje? 
Ocurre un conflicto con el contraint previamente establecido y no inserta el nuevo registro.
El mensaje indica esta violaci�n al constraint.

INTEGRIDAD REFERENCIAL
Se trata de una propiedad de una base de datos que establece que toda llave for�nea dentro de una tabla debe
hacer referencia a un registro existente dentro de la tabla referenciada.

