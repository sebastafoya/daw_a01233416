const prices = {
    i9: 11260.00,
    surface: 19999.00,
    quadro: 40936.00
}

let cart = {
    i9: 0,
    surface: 0,
    quadro: 0
}

let stock = {
    i9: 3,
    surface: 4,
    quadro: 6
}

let itemNames = {
    i9: "Intel Core i9",
    surface: "Microsoft Surface",
    quadro: "Nvidia Quadro P5000"
}

function currencyFormat(num) {
    return '$' + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

function updateTotals() {
    let subtotal = 0;
    for (const [id, count] of Object.entries(cart)) {
        subtotal += prices[id] * count;
    }
    const iva = subtotal*0.16;
    const total = subtotal + iva;
    
    document.getElementById("subtotalPriceTd").innerText = currencyFormat(subtotal);
    document.getElementById("IVAPriceTd").innerText = currencyFormat(iva);
    document.getElementById("totalPriceTd").innerText = currencyFormat(total);
}

function deleteCartRow(id) {
    const cartTable = document.getElementById("tbodyCart");
    const row = document.getElementById(id+"_cart_row");
    cartTable.removeChild(row);
    stock[id] += cart[id];
    cart[id] = 0;
    updateTotals();
}

function addItem(id) {
    if (stock[id] >= 1) {
        stock[id] -= 1;
        cart[id] += 1;

        if(cart[id] === 1) {
            const cartTable = document.getElementById("tbodyCart");
            const row = document.createElement("tr");
            row.id = id+"_cart_row";
    
            const itemTd = document.createElement("td");
            itemTd.classList = "left";
            itemTd.id = id+"_cart_name";
            itemTd.innerText = itemNames[id];
    
            const quantityTd = document.createElement("td");
            quantityTd.id = id+"_cart_quantity";
            quantityTd.innerText = cart[id];
            
            const priceTd = document.createElement("td");
            priceTd.id = id+"_cart_price";
            priceTd.innerText = currencyFormat(prices[id]*cart[id]);
    
            const deleteButton = document.createElement("button");
            deleteButton.classList = "deleteBtn";
            deleteButton.id = id;
            deleteButton.innerHTML = "Delete";
            deleteButton.addEventListener("click", function() {
                deleteCartRow(this.id);
                
            })
    
            const deleteTd = document.createElement("td");
            deleteTd.id = id+"_cart_delete";
            deleteTd.appendChild(deleteButton);
    
            row.appendChild(itemTd);
            row.appendChild(quantityTd);
            row.appendChild(priceTd);
            row.appendChild(deleteTd);
            cartTable.appendChild(row);
        } else {
            const quantityTd = document.getElementById(id+"_cart_quantity");
            quantityTd.innerText = cart[id];
    
            const priceTd = document.getElementById(id+"_cart_price");
            priceTd.innerText = currencyFormat(prices[id]*cart[id]);
        }
        updateTotals();
    } else {
        alert("Lo sentimos, el producto se ha agotado.")
    }
    
    
}

function setUp(){
    updateTotals();
}

setUp();