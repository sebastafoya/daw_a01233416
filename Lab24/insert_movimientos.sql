BEGIN;
  INSERT INTO TipoMovimiento VALUES('A', 'Retiro Cajero Automático');
  INSERT INTO TipoMovimiento VALUES('B', 'Deposito Ventanilla');
COMMIT;

BEGIN;
  INSERT INTO Movimiento (NoCuenta, ClaveM, Monto) VALUES('001', 'A', 500);
  UPDATE ClientesBanca SET Saldo = Saldo - 500 WHERE NoCuenta = '001';
COMMIT;
