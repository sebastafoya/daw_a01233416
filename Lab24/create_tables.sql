DROP TABLE IF EXISTS ClientesBanca CASCADE;
CREATE TABLE ClientesBanca (
  noCuenta VARCHAR(5) PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL,
  saldo NUMERIC(10,2) DEFAULT 0.00 NOT NULL
);

DROP TABLE IF EXISTS TipoMovimiento CASCADE;
CREATE TABLE TipoMovimiento (
  claveM VARCHAR(2) PRIMARY KEY,
  descripcion TEXT
);

DROP TABLE IF EXISTS Movimiento CASCADE;
CREATE TABLE Movimiento (
  noCuenta VARCHAR(5),
  claveM VARCHAR(2),
  fecha TIMESTAMP DEFAULT NOW(),
  monto NUMERIC(10,2) NOT NULL,
  FOREIGN KEY (noCuenta) REFERENCES ClientesBanca(noCuenta),
  FOREIGN KEY (claveM) REFERENCES TipoMovimiento(claveM),
  PRIMARY KEY (noCuenta, claveM, fecha)
);
