<?php
    function average() {
        if (isset($_POST['promedio'])) {
            $numbers = array($_POST["n1"], $_POST["n2"], $_POST["n3"], $_POST["n4"], $_POST["n5"], $_POST["n6"]);

            $sum = 0;
            $denum = 0;
            foreach ($numbers as $n) {
                if (is_numeric($n)){
                    $sum += $n;
                    $denum += 1;
                }
            }
            
            if ($denum > 0) {
                $avg = $sum / $denum;
            } else {
                $avg = "";
            }

            return $avg;
        }
    }

    function median() {
        if (isset($_POST['mediana'])) {
            $numbers = array($_POST["n1"], $_POST["n2"], $_POST["n3"], $_POST["n4"], $_POST["n5"], $_POST["n6"]);
            for ($i = 0; $i < 6; $i++) {
                if (!is_numeric($numbers[$i])) {
                    unset($numbers[$i]);
                }
            }
            sort($numbers);
            $mid = floor(count($numbers) / 2);

            if (count($numbers) > 0){
                $mediana = $numbers[$mid];
            } else {
                $mediana = "";
            }

            return $mediana;
        }
    }

    function array_to_string() {
        if (isset($_POST['lista'])) {
            $numbers = array($_POST["n1"], $_POST["n2"], $_POST["n3"], $_POST["n4"], $_POST["n5"], $_POST["n6"]);
            for ($i = 0; $i < 6; $i++) {
                if (!is_numeric($numbers[$i])) {
                    unset($numbers[$i]);
                }
            }

            if (count($numbers) > 0){
                $list = implode(',', $numbers);
            } else {
                $list = "";
            }
    
            return $list;
        }
        
    }

    function array_to_list() {
        if (isset($_POST['lista'])) {
            $numbers = array($_POST["n1"], $_POST["n2"], $_POST["n3"], $_POST["n4"], $_POST["n5"], $_POST["n6"]);
            for ($i = 0; $i < 6; $i++) {
                if (!is_numeric($numbers[$i])) {
                    unset($numbers[$i]);
                }
            }

            $sum = 0;
            $denum = 0;
            foreach ($numbers as $n) {
                $sum += $n;
                $denum += 1;
            }
            
            if ($denum > 0) {
                $avg = $sum / $denum;
            } else {
                $avg = "";
            }

            sort($numbers);
            $sorted_str = implode(',', $numbers);

            $mid = floor(count($numbers) / 2);

            if (count($numbers) > 0){
                $mediana = $numbers[$mid];
            } else {
                $mediana = "";
            }

            rsort($numbers);
            $rsorted_str = implode(',', $numbers);

            echo "<ul>\n";
            echo "<li>{$avg}</li>\n";
            echo "<li>{$mediana}</li>\n";
            echo "<li>{$sorted_str}</li>\n";
            echo "<li>{$rsorted_str}</li>\n";
            echo "</ul>\n";
        }
    }

    function tabla() {
        if (isset($_POST['tabla'])) {
            $number = $_POST["n"];

            echo "<table>";
            for ($i = 1; $i <= $number; $i++) {
                echo"<tr><td>";
                echo $i ** 2;
                echo "</td><td>";
                echo $i ** 3;
                echo "</td></tr>";
            }
            echo "</table>";
        }
    }

    function fibonacci() {
        if (isset($_POST['fibonacci'])) {
            $number = $_POST["n"];

            $n1 = 0;
            $n2 = 1;

            echo "<table>";
            for ($i = 1; $i <= $number; $i++) {
                $suma = $n1+$n2;
                $n1 = $n2;
                $n2 = $suma;
                echo"<tr><td>";
                echo $suma;
                echo "</td></tr>";
            }
            echo "</table>";
        }
    }
?>