<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <link rel="stylesheet" href="style.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Lab 9</title>
  </head>

  <nav class="salmon lighten-1 z-depth-3">
    <a href="#" class="brand-logo center">Lab 9</a>
  </nav>

  <?php
    require_once("functions.php");
  ?>

  <body>
    <div class="container">
        <h4>Una función que reciba un arreglo de números y devuelva su promedio...</h4>

        <div class="row">
            <form action="" method="post" class="col s12">
                <div class="input-field col s2">
                    <input type="text" name="n1">
                    <label for="n1">1</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n2">
                    <label for="n2">2</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n3">
                    <label for="n3">3</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n4">
                    <label for="n4">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n5">
                    <label for="n5">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n6">
                    <label for="n6">6</label>
                </div>
                <button class="btn orange lighten-1" type="submit" name="promedio">Promedio</button>
            </form>
        </div>

        <div class="container center">
            <div class='chip hoverable yellow'>
                <?php 
                    echo average();
                ?>
                <i class='material-icons'>lightbulb_outline</i>
            </div>
        </div>

        <h4>Una función que reciba un arreglo de números y devuelva su mediana...</h4>

        <div class="row">
            <form action="" method="post" class="col s12">
                <div class="input-field col s2">
                    <input type="text" name="n1">
                    <label for="n1">1</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n2">
                    <label for="n2">2</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n3">
                    <label for="n3">3</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n4">
                    <label for="n4">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n5">
                    <label for="n5">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n6">
                    <label for="n6">6</label>
                </div>
                <button class="btn orange lighten-1" type="submit" name="mediana">Mediana</button>
            </form>
        </div>

        <div class='container center'>
            <div class='chip hoverable yellow'>
                <?php 
                    echo median();
                ?>
                <i class='material-icons'>lightbulb_outline</i>
            </div>
        </div>

        <h4>Una función que reciba un arreglo de números y muestre la lista de números, y como ítems de una lista html muestre el promedio, la media, y el arreglo ordenado de menor a mayor, y posteriormente de mayor a menor...</h4>

        <div class="row">
            <form action="" method="post" class="col s12">
                <div class="input-field col s2">
                    <input type="text" name="n1">
                    <label for="n1">1</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n2">
                    <label for="n2">2</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n3">
                    <label for="n3">3</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n4">
                    <label for="n4">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n5">
                    <label for="n5">5</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="n6">
                    <label for="n6">6</label>
                </div>
                <button class="btn orange lighten-1" type="submit" name="lista">Lista</button>
            </form>
        </div>

        <div class='container center'>
            <div class='chip hoverable yellow'>
                <?php 
                    echo array_to_string();
                ?>
                <i class='material-icons'>lightbulb_outline</i>
            </div>
        </div>

        <div class='container center'>
            <ul>
                <li><?php array_to_list(); ?></li>
            </ul>
        </div>

        <h4>Una función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n...</h4>

        <div class="row">
            <form action="" method="post" class="col s12">
                <div class="input-field">
                    <input type="text" name="n">
                    <label for="n">Ingresa un número</label>
                </div>
                <button class="btn orange lighten-1" type="submit" name="tabla">Tabla</button>
            </form>
        </div>

        <div class='container center'>
            <ul>
                <li><?php tabla(); ?></li>
            </ul>
        </div>

        <h4>Algún problema que hayas implementado en otro lenguaje de programación...</h4>

        <div class="row">
            <form action="" method="post" class="col s12">
                <div class="input-field">
                    <input type="text" name="n">
                    <label for="n">Ingresa un número</label>
                </div>
                <button class="btn orange lighten-1" type="submit" name="fibonacci">Fibonacci</button>
            </form>
        </div>

        <div class='container center'>
            <ul>
                <li><?php fibonacci(); ?></li>
            </ul>
        </div>

        <h4>Preguntas</h4>

        <h5>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h5>
        <p>
            Muestra información sobre la configuración de PHP<br>
            Muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP.<br>
            Como cada sistema se instala diferente phpinfo() se usa comúnmente para revisar opciones de configuración y variables predefinidas disponibles en un sistema dado<br>
            phpinfo() también es una valiosa herramienta de depuración ya que contiene todos valores EGPCS (Environment, GET, POST, Cookie, Server).
        </p>

        <h5>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción? </h5>
        <p>
            Se deben preparar las condiciones apropiadas de seguridad, como usuarios de la base de datos protegidos con contraseñas, un firewall, etc.
        </p>

        <h5>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h5>
        <p>
            Al recibir un request, el servidor responde con el código HTML tal como fue enviado, pero procesa las declaraciones que contengan código PHP.
        </p>

    </div>
    <!--JavaScript at end of body for optimized loading-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>M.AutoInit()</script>
  </body>
</html>