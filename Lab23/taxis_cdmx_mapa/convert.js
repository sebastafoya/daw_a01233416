const csvToJson = require('convert-csv-to-json');

const input = './mex_clean.csv';
const output = './public/mex_clean.json';

csvToJson.fieldDelimiter(',').formatValueByType().generateJsonFileFromCsv(input, output);